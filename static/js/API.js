(()=>{
  function updateControls(url, property, defval, autoselect=false){
    const $target = $("#select-"+property);

    // Prevent repeating updates
    if($target.attr('data-currentData') == url) return $.Deferred().resolve();
    $target.attr('data-currentData', url);

    $("#rozvrh table").stop().fadeOut("fast").promise().then(function(){
      $(this).parent().html("");
    });
    $("#timer-content").html("");

    toggleControls(false);
    return $.get(url, {}, d=>d, "json")
    .then(data=>{
      data = data[property].sort();
      // Remove everything but placeholder
      $("option[value!='']", $target).remove();
      for(const item of data){
        $target.append(
          $("<option>").text(item).attr("value", item)
        );
      }
      if(defval) $target.val(defval);
      if(!$target.val() && (autoselect || data.length == 1)){
        $target.val(data[0]);
      }
      $target.change().trigger("zhin.updated");
    })
    .always(()=>{
      toggleControls(true);
    });
  }

  let lastParams = [];
  const landscapeMQ = window.matchMedia("(orientation: portrait)");
  landscapeMQ.onchange = ()=>{
    if(lastParams.length) API.render(...lastParams);
  };


  const API = {
    rozvrhData: {},

    // get list of groups
    getGroups(className, defval){
      if(!className) return $.Deferred().resolve();
      return updateControls("/api/"+className, "groups", defval);
    },

    // get list of weeks
    getWeeks(className, groupName, defval){
      if(!className || !groupName) return $.Deferred().resolve();
      return updateControls(
        "/api/"+className+"/"+groupName, "weeks", defval, true
      );
    },

    // updates rozvrh
    render(className, groupName, week){
      lastParams = [className, groupName, week];
      const isMobile = landscapeMQ.matches;

      toggleControls(false);
      return $.get("/api/"+className+"/"+groupName+"/"+week+"?render=true", {},
        d=>d, "json").then((data)=>{
        // render
        $("#rozvrh table").stop();
        $("#rozvrh").html(data[isMobile ? "mobileHtml" : "html"])
        $("#rozvrh table").fadeIn("fast");
        this.rozvrhData = data;
        updateTimings();
      }).always(()=>{
        toggleControls(true);
      });
    }
  }
  window.API = API;
})();
