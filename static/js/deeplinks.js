(()=>{
  const UPDATE_DELAY = 50;
  let updateTimeout, lastHash, forceUpdate=false;

  function updateHash(className, groupName, week, force=false){
    // add fallback for single-grouped classes
    const _groupName = className == groupName ? "" : groupName;
    // build deeplink url
    const url = [className, _groupName, week].map(v=>v||"");
    // remove // or //A from deeplink as it's just defaults
    const hash = "#"+url.join("/").replace(/\/+A?$/, "");
    // update if new
    if(force || hash != window.location.hash.replace(/\/+A?$/, "")){
      if(force) {
        forceUpdate = true;
        window.location.hash = '#';
      }
      window.location.hash = hash;
      return true;
    }
    return false;
  }
  window.updateHash = updateHash;


  function hashUpdate(event){
    if(event) event.preventDefault();
    if(updateTimeout) clearTimeout(updateTimeout);

    updateTimeout = setTimeout(()=>{
      updateTimeout = null;
      if(lastHash == window.location.hash && !forceUpdate) return;
      lastHash = window.location.hash;

      const sanitarizedHash = window.location.hash.replace(/^#+\/*/, "");
      let [className,groupName,week] = sanitarizedHash.split("/");
      if(!className ) return;
      if(!groupName ) groupName = className;
      if(!week      ) week = "A";

      window.localStorage.setItem('lastHash', sanitarizedHash);

      // Update the controls
      window.updateControls([className,groupName,week])
        // Update rozvrh
        .then(()=>window.API.render(className, groupName, week));
    }, UPDATE_DELAY);
  }

  const savedHash = window.localStorage.getItem('lastHash');
  if((window.location.hash || "").replace(/^#/, "") == "" && savedHash) {
    window.location.hash = "#"+savedHash;
  }
  hashUpdate();
  $(window).on('hashchange', hashUpdate);
})();
