(()=>{
  let className, groupName, weekName;

  const getGroups = () =>window.API.getGroups(className, groupName);
  const getWeeks  = () =>window.API.getWeeks(className, groupName, weekName);
  const getRozvrh = (f)=>window.updateHash(className, groupName, weekName, f);

  window.forceUpdateRender = ()=>getRozvrh(true);

  // Download list of groups
  const classSelect = $("#select-classes").change(function(){
    if($(this).val() == className) return;
    className = $(this).val();
    if(!className || className == "") return;

    getGroups();
  });
  if(classSelect.children().length < 2) classSelect.change();

  // Update weeks
  $("#select-groups").change(function(){
    groupName = $(this).val();
    if(!groupName) return;

    getWeeks();
  });

  // Update URL
  $("#select-weeks").change(function(){
    weekName = $(this).val();
    if(!weekName || weekName == "") return;

    getRozvrh();
  });


  function updateControls(data){
    [className, groupName, weekName] = data;

    $("#select-classes").val(className).change();
    return getGroups().then(()=>{
      $("#select-groups").val(groupName).change();
      return getWeeks();
    }).then(()=>{
      $("#select-weeks").val(weekName).change();
    });
  }
  window.updateControls = updateControls;


  const ENABLE_CONTROLS_DELAY = 100;
  let enableControlsTimeout;

  function toggleControls(enabled=true){
    if(enableControlsTimeout) clearTimeout(enableControlsTimeout);

    if(!enabled) _run();
    else enableControlsTimeout = setTimeout(_run, ENABLE_CONTROLS_DELAY);

    function _run(){
      enableControlsTimeout = null;
      $("#loader").toggleClass("hide", enabled);
      $(".controls").toggleClass("disabled", !enabled).each(function(){
        // Disable single and no value selects
        if($("option", this).length <= 2){
          $(this).addClass("disabled");
        }
        if(!$("select", this).val()) $(this).dropdown('clear');
      });
    }
  }
  window.toggleControls = toggleControls;
  toggleControls();
})();


function updateTimings(){
  if($("#rozvrh").is(":empty")) return;

  // reset
  $("#rozvrh .highlight").removeClass("highlight");
  $("#rozvrh .current").removeClass("current");
  $("#rozvrh .following").removeClass("following");
  $("#rozvrh .prehighlight").removeClass("prehighlight");
  $("#timer-content").html("");

  const date = new Date();
  // date.setHours(12, 40, 00);
  const day = date.getDay();
  $("#rozvrh [data-day='"+day+"']").addClass("highlight");
  const days = API.rozvrhData.week;
  const dayData = days[day-1];
  const lessonTimes = API.rozvrhData.lessonTimes;

  let lastEndTime, isBreak, nextDay;

  (()=>{
    if(!dayData) return;
    const firstHour = getTimerange(lessonTimes[0])[0];
    const lastHour  = getTimerange(lessonTimes[dayData.length-1])[1];

    if (date > lastHour) return;
    else if(date < firstHour){
      isBreak = true;  lastEndTime = firstHour;
      return;
    }

    for(const hour in dayData){
      const attr = API.rozvrhData.lessonTimes[hour];
      const timerange = getTimerange(attr);

      // This is the ongoing lesson
      if(date >= timerange[0] && date <= timerange[1]) {
        $("#rozvrh [data-time='"+attr+"']").addClass("highlight");
        $(
          "#rozvrh [data-day='"+day+"'][data-time='"+attr+"']"
        ).addClass("current");
        isBreak = false;  lastEndTime = timerange[1];
        break;
      }
      // This lesson will come after the break
      else if(lastEndTime && date > lastEndTime && date < timerange[0]){
        $("#rozvrh [data-time='"+attr+"']").addClass("prehighlight");
        $(
          "#rozvrh [data-day='"+day+"'][data-time='"+attr+"']"
        ).addClass("following");
        isBreak = true;  lastEndTime = timerange[0];
        break;
      }

      lastEndTime = timerange[1];
    }
  })();

  if(!lastEndTime){
    let _day=day-1, i=0;
    // find next schooling day
    while(nextDay === undefined && i < 10){
      const _nextDay = ++_day % days.length;
      if(_nextDay < days.length){
        for(const [hour,lessons] of days[_nextDay].entries()){
          if(lessons.length){
            lastEndTime = getTimerange(lessonTimes[hour])[0];
            nextDay = _nextDay;
            break;
          }
        }
      }
      i++;
    }
    if(nextDay === undefined) throw Error("Finding next schooling day failed");
  }

  // Update time
  if(nextDay !== undefined){
    const $newContent = $($("#timer-nextDay").prop("content")).clone();
    const nextDate = new Date();
    if(nextDay < day) nextDay += 7;
    const dayDelta = nextDay - (day-1);
    nextDate.setTime(nextDate.getTime() + dayDelta*24*60*60*1000);
    nextDate.setHours(
      lastEndTime.getHours(), lastEndTime.getMinutes(), lastEndTime.getSeconds()
    );
    $("span", $newContent).text(nextDate.toLocaleString());
    $("#timer-content").html($newContent);
  }
  else{
    const $newContent = $(
      (isBreak ? $("#timer-nextClass") : $("#timer-nextBreak")).prop("content")
    ).clone();
    const deltaDate = new Date(lastEndTime - date);
    const deltaDays = Math.floor(deltaDate.getTime() / (24*60*60*1000));
    let wasLastZero = true;
    const time = [
      [deltaDays, "d"],
      [deltaDate.getHours()-1, "h"],
      [deltaDate.getMinutes(), "m"],
      [deltaDate.getSeconds(), "s"]
    ].map((v,i,a)=>{
      if(wasLastZero && v[0]==0 && i != a.length-1) return;
      else wasLastZero = false;
      if(i > 0) v[0] = Math.abs(v[0]); return v.join("").padStart(3, "0");
    }).filter(v=>v).join(" ");
    $("span", $newContent).text(time);
    $("#timer-content").html($newContent);
  }


  function getTimerange(attr){
    return attr.split(" - ").map(time=>{
      const date = new Date();
      date.setHours(...time.split(":").map(v=>Number(v)), 0);
      return date;
    });
  }
}
setInterval(updateTimings, 1000);


if('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/static/js/serviceWorker.js', {
    scope: "/"
  });

  navigator.serviceWorker.addEventListener('message', event => {
    if(event.data == "CONTENT_UPDATED"){
      window.forceUpdateRender();
    }
  });

  navigator.serviceWorker.ready.then(()=>{
    $("#offlineInfo").show();
  })
}

let installPrompt;

$(window)
  .on('beforeinstallprompt', (event)=>{
    installPrompt = event.originalEvent;
    $("#installButton").show();
  })
  .on('appinstalled', ()=>{
    $("#installButton").hide();
  })
  .on('online', ()=>{
    $("#offlineWarn").addClass("hide");
    window.forceUpdateRender();
  })
  .on('offline', ()=>{
    $("#offlineWarn").removeClass("hide");
  });
$("#offlineWarn").toggleClass("hide", navigator.onLine);

$("#installButton").click(()=>{
  installPrompt.prompt();
  
  installPrompt.userChoice.then((choiceResult) => {
    if (choiceResult.outcome === 'accepted') {
      $("#installButton").hide();
    }
  });
});

// Detect touch device
if('ontouchstart' in window || 'onmsgesturechange' in window){
  $('.ui.dropdown').fullscreenDropdown();
}else{
  $('.ui.dropdown').dropdown();
}
