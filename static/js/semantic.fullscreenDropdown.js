$.fn.fullscreenDropdown = function(options={}){
  let fullscreenWrap;
  this.dropdown({
    ...options,
    onShow(){
      if(fullscreenWrap) return false;
      fullscreenWrap = true;
      const $that = $(this);
      if(options.onShow) options.onShow();

      fullscreenWrap = $('<div>', {class: "fullscreenDropdown-wrap"})
        .append(
          $('<div>', {class: 'ui dropdown selection active visible'})
          .append(
            $(".text", $(this).parent()).clone()
            .addClass('default')
            .text($("[value='']", this).text())
          )
          .append(
            $(".menu", $(this).parent()).clone().addClass('visible')
            .css('display', 'block')
            .on('click', '.item', {}, function(){
              $that.val($(this).attr('data-value')).change();
            })
          )
        )
        .appendTo(document.body)
        .click(()=>{
          fullscreenWrap.removeClass('active').one('transitionend', ()=>{
            fullscreenWrap.remove();  fullscreenWrap = null;
          });
        });

      setTimeout(()=>{
        $(".dropdown", fullscreenWrap).css('margin-bottom',
          $(".menu", fullscreenWrap).height()
        ).scrollTop($(".menu .selected", fullscreenWrap).length
          ? $(".menu .selected", fullscreenWrap).offset().top
          : 0
        );
        fullscreenWrap.addClass('active');
      }, 10);

      return false;
    }
  });
};
