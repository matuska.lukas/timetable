const YAML  = require('yaml');
const fs    = require('fs');

const ROZVRHYDIR = "rozvrhy/";

module.exports = function getRozvrhs(){
  const rozvrhy = {};
  const dir = fs.readdirSync(ROZVRHYDIR);

  for(const classFileName of dir){
    if(classFileName.startsWith("_") || !classFileName.endsWith(".yml")) return;

    const classFile = fs.readFileSync(ROZVRHYDIR+classFileName).toString();
    const classData = YAML.parse(classFile);
    const className = classFileName.replace(/\.yml$/,"");

    const classRozvrh = rozvrhy[className] = {};
    const weeks = [];

    //// Get all groups and weeks
    function addGroupsAndWeeks(lesson){
      const {groups,week} = lesson;

      if(week && !weeks.includes((week))) weeks.push(week);

      (groups || []).forEach((group)=>{
        if(!(group in classRozvrh)) classRozvrh[group] = {};
      });
    }

    traverseRozvrh(classData, addGroupsAndWeeks);

    /// fallbacks for simpler rozvrhs
    // No groups
    if(!Object.keys(classRozvrh).length) classRozvrh[className] = {};
    // No weeks
    if(!weeks.length) weeks.push("A");

    //// Generate rozvrhy for each
    for(const groupName in classRozvrh){
      const group = classRozvrh[groupName];
      weeks.forEach(week=>group[week]=[]);
      let dayNumber = -1;

      function switchDay() { dayNumber++; }

      function processLesson(lesson, index){
        if(lesson.groups && !lesson.groups.includes(groupName)) return;

        function addLesson(week){
          while(week.length <= dayNumber) week.push([]);
          const day = week[dayNumber];
          while(day.length <= index) day.push([]);
          day[index].push(lesson);
        }

        if(lesson.week){
          addLesson(group[lesson.week])
        }else{
          Object.values(group).forEach(addLesson);
        }
      }

      traverseRozvrh(classData, processLesson, switchDay);
    }
  }

  return rozvrhy;
}


function traverseRozvrh(rozvrh, callback=()=>{}, dayCallback=()=>{}){
  for(const day of rozvrh){
    dayCallback();
    for(let i = 0; i < day.length; i++){
      const lesson = day[i];
      if(!lesson) continue;
      if(Array.isArray(lesson)){
        lesson.forEach(sublesson=>callback(sublesson, i));
      }
      else callback(lesson, i);
    }
  }
}
