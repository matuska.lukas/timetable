const express     = require('express');
const bodyParser  = require('body-parser');
const helmet      = require('helmet');
const nunjucks    = require('nunjucks');
const fs          = require('fs');
const CONFIG    = require("./config.json");

const MANIFEST  = JSON.parse(
  fs.readFileSync("./static/manifest.webmanifest").toString()
);

const rozvrhs = require('./rozvrhParser.js')();
const parsedRozrvrhs = {};

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(helmet());
app.use("/static/manifest.webmanifest", (_,res,next)=>{
  res.set("Content-Type", "application/manifest+json");
  next();
});
app.use("/static/js", (_,res,next)=>{
  res.set("Service-Worker-Allowed", "/");
  next();
});
app.use("/static", express.static("static"));
nunjucks.configure("views", {
  autoescape: true,
  express   : app,
  watch     : !global.isProduction
});

app.get("/", (_,res)=>{
  res.render("index.twig", {classes: Object.keys(rozvrhs), MANIFEST});
});

// Generate urls
for(const className in rozvrhs){
  const parsedClassData = parsedRozrvrhs[className] = {};
  const classData = rozvrhs[className];
  const groups = Object.keys(classData);

  app.get(`/api/${className}`, (_,res)=>{
    res.json({
      groups, className
    });
  });

  for(const groupName in classData){
    const parsedGroupData = parsedClassData[groupName] = {};
    const groupData = classData[groupName];

    app.get(`/api/${className}/${groupName}`, (_,res)=>{
      res.json({
        weeks: Object.keys(groupData), className, groupName
      });
    });

    for(const [weekName, week] of Object.entries(groupData)){
      const lengths = [];
      const subjects = [];
      const subjectColors = {};
      const lowestIndexes = [];
      for(const day of week){
        lengths.push(day.length);
        let checkedIndex = false;
        for(const [i,lessons] of day.entries()){
          if(!checkedIndex && lessons.length){
            lowestIndexes.push(i);
            checkedIndex = true;
          }
          for(const {subject} of lessons){
            if(!subjects.includes(subject)) subjects.push(subject);
          }
        }
      }

      const maxLength = Math.max(...lengths);
      const lowestIndex = Math.min(...lowestIndexes);
      // Subject colors
      for(const [index,subject] of subjects.entries()){
        subjectColors[subject] = "hsl("+
          Math.floor(index / subjects.length * 360)+"deg,"+
          "65%, 55%)";
      }
      const context = parsedGroupData[weekName] = {
        week, subjects, className, groupName, subjectColors, ...CONFIG,
        maxLength, lowestIndex
      };
      context.html = nunjucks.render("rozvrh.twig", context);
      context.mobileHtml = nunjucks.render("rozvrh-mobile.twig", context);

      app.get(`/api/${className}/${groupName}/${weekName}`, (req,res)=>{
        if(req.query.render) {
          res.json(context);
        }
        else res.json(context);
      });
    }
  }
}

app.get('/api/_fullData', (_,res)=>{
  res.json(parsedRozrvrhs);
});

const server = app.listen(CONFIG.port, CONFIG.host, ()=>{
  const addr = server.address();
  const url = (typeof addr == "string"
    ? addr
    : `http://${addr.address}:${addr.port}/`
  );
  console.info("Listening on", url);
});
